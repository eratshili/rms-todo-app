import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../components/confirm-dialog/confirm-dialog.component';

@Injectable()
export class ConfirmDialogService {
  dialogRef: any;
  constructor(public dialog: MatDialog) { }

  openDialog(type, data) {
    return this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      data: { name: data.task, type: type }
    });
  }

  openConfirmDialog(type, data) {
    this.dialogRef = this.openDialog(type, data);

    return this.dialogRef.afterClosed()
  }
}
