import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from 'angularfire2/firestore';
import { MatSnackBar } from '@angular/material';
import { ConfirmDialogService } from './dialogs/confirm-dialog.service';

@Injectable()
export class DataProvidersService {

  tasksDB: AngularFirestoreCollection<any> = this.afs.collection('tasks');
  historyDB: AngularFirestoreCollection<any> = this.afs.collection('history');

  constructor(
    private afs: AngularFirestore,
    public confirmDialog: ConfirmDialogService,
    public snackBar: MatSnackBar
  ) {}


  getAccounts() {
    return this.tasksDB;
  }

  addHistory(data) {
    return this.historyDB.add(data).then(res => {
      res.update({ id: res.id });
    })
  }

  addTask(data) {
    return this.tasksDB.add(data).then(res => {
      console.log(res.id);
      res.update({ id: res.id });
      
    this.addHistory({taskId: res.id, action: 'ADD', date: new Date() });
      this.snackBar.open('Task was added', 'CLOSE', {
        duration: 2000,
      });
    })
  }

  updateTask(data) {
    return this.tasksDB.doc(data.id).update(data).then(a => {
      this.snackBar.open('Task was updated', 'CLOSE', {
        duration: 2000,
      });
    })
  }


  deleteTask(type, data) {
    this.confirmDialog.openConfirmDialog(type, data).subscribe(res => {
      if(res) {
        return this.tasksDB.doc(data.id).delete().then(a => {
          this.snackBar.open(type + ' deleted', 'CLOSE', {
            duration: 2000,
          });
        });
      }
    })
  }

  getTodoTasks() {
    return this.afs.collection('tasks', ref => ref.where('status', '==', 'TODO'));
  }

  getInProgressTasks() {
    return this.afs.collection('tasks', ref => ref.where('status', '==', 'INPROGRESS'));
  }

  getCompletedTasks() {
    return this.afs.collection('tasks', ref => ref.where('status', '==', 'COMPLETED'));
  }

  getTaskHistory(id) {
    return this.afs.collection('history', ref => ref.where('taskId', '==', id));
  }

}
