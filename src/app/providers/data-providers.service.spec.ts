import { TestBed, inject } from '@angular/core/testing';

import { DataProvidersService } from './data-providers.service';

describe('DataProvidersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DataProvidersService]
    });
  });

  it('should be created', inject([DataProvidersService], (service: DataProvidersService) => {
    expect(service).toBeTruthy();
  }));
});
