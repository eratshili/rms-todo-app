import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { DataProvidersService } from '../../providers/data-providers.service';

@Component({
  selector: 'app-task-history',
  templateUrl: './task-history.component.html',
  styleUrls: ['./task-history.component.scss']
})
export class TaskHistoryComponent implements OnInit {

  taskHistory: any;
  constructor(
    public dialogRef: MatDialogRef<TaskHistoryComponent>,
    public dataServices: DataProvidersService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      dataServices.getTaskHistory(data.id).valueChanges().subscribe((res) => {
        this.taskHistory = res;
        // console.log(res);
      })
    }

  ngOnInit() {
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
