import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { DataProvidersService } from './../../../../providers/data-providers.service';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AddTaskComponent implements OnInit {

  competitionTypes: any;

  taskStatuseData = [
    {name: 'Todo', value: 'TODO'},
    {name: 'In Progress', value: 'INPROGRESS'},
    {name: 'Completed', value: 'COMPLETED'}
  ];
  constructor(
    public dialogRef: MatDialogRef<AddTaskComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public compService: DataProvidersService) {}

  ngOnInit() {
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
