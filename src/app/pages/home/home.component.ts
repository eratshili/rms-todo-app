import { DataProvidersService } from './../../providers/data-providers.service';
import { AddTaskComponent } from './modals/add-task/add-task.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireDatabase } from 'angularfire2/database';
import { TaskHistoryComponent } from '../../components/task-history/task-history.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  loadingCompletedTasks: boolean;
  loadinginProgressTasks: boolean;
  loadingTodosTasks: boolean;
  type: string = 'task';
  todosTasksData: any[];
  inProgressTasksData: any[];
  completedTasksData: any[];
  
  constructor(
    public dialog: MatDialog,
    private afs: AngularFirestore,
    private af: AngularFireDatabase,
    public dataService: DataProvidersService
  ) {
  }

  ngOnInit() {
    this.loadCompletedTasks();
    this.loadTodosTasks();
    this.loadInProgressTasks();
  }

  loadTodosTasks() {
    this.loadingTodosTasks = true;
    this.dataService.getTodoTasks().valueChanges().subscribe((data) => {
      this.todosTasksData = data;
      console.log(data);
      this.loadingTodosTasks = false;
    })
  }

  loadInProgressTasks() {
    this.loadinginProgressTasks = true;
    this.dataService.getInProgressTasks().valueChanges().subscribe((data) => {
      this.inProgressTasksData = data;
      console.log(data);
      this.loadinginProgressTasks = false;
    })
  }

  loadCompletedTasks() {
    this.loadingCompletedTasks = true;
    this.dataService.getCompletedTasks().valueChanges().subscribe((data) => {
      this.completedTasksData = data;
      console.log(data);
      this.loadingCompletedTasks = false;
    })
  }

  openViewHistory(item) {
    let dialogRef = this.dialog.open(TaskHistoryComponent, {
      width: '500px',
      data: item
    });

    dialogRef.afterClosed()
  }

  openAddTaskDialog() {
    let dialogRef = this.dialog.open(AddTaskComponent, {
      width: '500px',
      data: { id: '', task: '', description: '', status: '', date: new Date(), priority: 0 }
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.addTask(result);
        
        this.loadCompletedTasks();
        this.loadTodosTasks();
        this.loadInProgressTasks();
      }
    });
  }


  openEditTaskDialog(item) {
    console.log(item);
    let dialogRef = this.dialog.open(AddTaskComponent, {
      width: '500px',
      data: item
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.updateTask(result);

        this.loadCompletedTasks();
        this.loadTodosTasks();
        this.loadInProgressTasks();
      }
    });
  }

  updateTaskStatus(data, status) {
    let tempData = data;
    tempData.status = status;
    this.dataService.updateTask(tempData);
    this.dataService.addHistory({taskId: tempData.id, action: 'MOVED', to: status, date: new Date() });
  }

  addTask(res) {
    this.dataService.addTask(res);
  }

  updateTask(res) {
    this.dataService.updateTask(res);
    this.dataService.addHistory({taskId: res.id, action: 'UPDATED', date: new Date() });
  }

  deleteTask(item) {
    this.dataService.deleteTask(this.type, item);
    this.dataService.addHistory({taskId: item.id, action: 'DELETED', date: new Date() });
  }

}
